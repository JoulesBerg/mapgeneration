You will need the powershell terminal to use this on windows. You should be able to google how to install that if you do not already have it.

clone the repository to wherever you want it, then enter the following commands in powershell after you have the correct folder open in powershell using the cd command

python -m venv .venv
.venv/scripts/activate
pip install -r requirements.txt
code .


for both matrix generation methods you need to enter a file name, the default is example.txt, which you can edit close to the bottom of the files.

matrixgen generates a matrix using almost entirely CPU power, and is slow consequentially, but this makes its only real limitation in time: 
theoretically you could make an extremely large matrix, though eventually you would run out of ram(albeit at some absolutely absurd matrix size)

classmatrixgen generates a matrix using almost the same math, but uses class objects to reduce the overall number of CPU calculations at the cost of using a significant
amount of ram for larger matrixes. It is about 4 times as fast as the vanilla generator, though I have not done extensive testing.

count takes a text file name you enter manually and returns the number of tiles of a given tier in that map text file

to render your matrix make sure your virtual environment is active (.venv/scripts/activate if you did not do this earlier -- 
you can type 'deactivate' to turn it off at any time)
enter the command 

jupyter-lab

then open the imagegen.ipynb in the page that will open. select the matrix file you want to render by editing the file name near the bottom
you should also specify a file output name.

run it.
 
large matrixes take a lot of ram to render, an 8000 by 8000 took about 7Gb of ram and several minutes.
 

Sidenote: All generated matrices should be tileable. I coded hexes to border the opposite edge/corner if they were on the edge of the matrix.