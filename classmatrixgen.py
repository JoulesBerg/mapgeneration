from random import randint
import time
#the class matrix generator is three times as fast as the vanilla matrix generator, though mathematically they are exactly the same.
#this is because the class matrix creates every position on the grid as a class object, so many calculations are able to be skipped at the cost of RAM usage.
#RAM is the primary limiter of this matrix generation tool. A matrix that is 2000 by 2000 uses aporximately 4gb of ram continuously during the generation process. 
#

class Position:
    def __init__(self,row,column,gridsize):
        self.row = row
        self.column = column
        self.gridsize = gridsize
        if gridsize%2==0:
            self.gridheight = gridsize
        else:
            self.gridheight = gridsize+1
        self.bordering = self.get_bordering(row,column)
        self.tier = 0
        
    def get_bordering(self, row, column):
        bordering = []
        if row ==0:
            up = self.gridheight-1
            down = row+1
            c1 = column
            if column == 0:
                    c2 = self.gridsize-1
            else:
                c2= column-1
        elif row == self.gridheight-1:
            up = row-1
            down = 0
            c1 = column
            if column == self.gridsize-1:
                c2 = 0
            else:
                c2=column+1   
        else:
            if row%2==0:
                up = row-1
                down = row+1
                c1 = column
                if column == 0:
                    c2 = self.gridsize-1
                else:
                    c2= column-1
            else:
                up = row-1
                down = row+1
                c1 = column
                if column == self.gridsize-1:
                    c2 = 0
                else:
                    c2=column+1     
        if column ==0:
            right = column +1
            left = self.gridsize-1
        elif column == self.gridsize-1:
            right = 0
            left = column-1
        else:
            right = column +1
            left = column-1
        bordering.append((up,c1))
        bordering.append((up,c2))
        bordering.append((row,left))
        bordering.append((row,right))
        bordering.append((down,c1))
        bordering.append((down,c2))
        return bordering
    def __repr__(self):
        return str(self.tier)


def emptyrow():
    return []


def timeformat(inputseconds):
    milliseconds = int(inputseconds*100)
    seconds = milliseconds//100
    minutes = seconds//60
    hours = minutes//60
    minutesleft = minutes%60
    secondsleft = seconds%60
    millisecondsleft = milliseconds%100
    return f"{hours}:{minutesleft}:{secondsleft}:{millisecondsleft}" 


class Hexmap():
    def __init__(self, size, dtier):
        self.size = size
        self.grid = []
        self.dtier = dtier
        if size%2==0:
            self.height = size
        else:
            self.height = size+1
        for i in range(self.height):
            self.grid.append(emptyrow())
        for row in self.grid:
            for i in range(self.size):
                row.append(Position(self.grid.index(row),i,self.size))

    def evolve(self):
        high = 0
        passcount = 0
        while high < self.dtier:
            passcount+=1
            rowcount=0
            for row in self.grid:
                rowcount+=1
                for pos in row:
                    chance = randint(1,100)
                    if chance>98:
                        pos.tier+=1
                        if pos.tier>high:
                            high+=1
                    for (brow,bcol) in pos.bordering:
                        while pos.tier - self.grid[brow][bcol].tier>2:
                            self.grid[brow][bcol].tier+=1
                formattedtime = timeformat(time.process_time())
                print(f"Pass {passcount}: Evolution of row {rowcount} finished at {formattedtime}")



newmap = Hexmap(100,8)
newmap.evolve()

out = open("example.txt", "w")
for line in newmap.grid:
  # write line to output file
  linestr = str(line)
  out.write(linestr)
  out.write(",")
  out.write("\n")
out.close()

