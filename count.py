from imagegen import readgrid

grid = readgrid('test9matrix.txt')



zerocount = 0
onecount = 0
twocount = 0
threecount=0
fourcount=0
fivecount=0
sixcount=0
sevencount=0
eightcount=0
ninecount=0
tencount=0
elevencount=0
twelvecount=0
thirteencount=0
fourteencount=0
fifteencount = 0


for row in grid:
    for item in row:
        if item ==0:
            zerocount+=1
        elif item ==1:
            onecount+=1
        elif item ==2:
            twocount+=1
        elif item ==3:
            threecount+=1
        elif item==4:
            fourcount+=1
        elif item==5:
            fivecount+=1
        elif item==6:
            sixcount+=1
        elif item==7:
            sevencount+=1
        elif item==8:
            eightcount+=1
        elif item ==9:
            ninecount+=1
        elif item==10:
            tencount+=1
        elif item==11:
            elevencount+=1
        elif item==12:
            twelvecount+=1
        elif item ==13:
            thirteencount+=1
        elif item ==14:
            fourteencount+=1
        elif item == 15:
            fifteencount+=1


print(f"T0: {zerocount}")
print(f"T1: {onecount}")
print(f"T2: {twocount}")
print(f"T3: {threecount}")
print(f"T4: {fourcount}")
print(f"T5: {fivecount}")
print(f"T6: {sixcount}")
print(f"T7: {sevencount}")
print(f"T8: {eightcount}")
print(f"T9: {ninecount}")
print(f"T10: {tencount}")
print(f"T11: {elevencount}")
print(f"T12: {twelvecount}")
print(f"T13: {thirteencount}")
print(f"T14: {fourteencount}")
print(f"T15: {fifteencount}")