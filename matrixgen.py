from statistics import mean
from random import randint
import time

def makerow(size):
    row = []
    for i in range(size):
            row.append(0)
    return row

class HexMap():
    def __init__(self, size):
        self.size = size
        self.grid = []
        if size%2==0:
            self.height = size
        else:
            self.height = size+1
        for i in range(self.height):
            self.grid.append(makerow(size))
        
            
    def get_bordering(self, row, column):
        bordering = []
        if row ==0:
            up = self.height-1
            down = row+1
            c1 = column
            if column == 0:
                    c2 = self.size-1
            else:
                c2= column-1
        elif row == self.height-1:
            up = row-1
            down = 0
            c1 = column
            if column == self.size-1:
                c2 = 0
            else:
                c2=column+1   
        else:
            if row%2==0:
                up = row-1
                down = row+1
                c1 = column
                if column == 0:
                    c2 = self.size-1
                else:
                    c2= column-1
            else:
                up = row-1
                down = row+1
                c1 = column
                if column == self.size-1:
                    c2 = 0
                else:
                    c2=column+1     
        if column ==0:
            right = column +1
            left = self.size-1
        elif column == self.size-1:
            right = 0
            left = column-1
        else:
            right = column +1
            left = column-1
        bordering.append(self.grid[up][c1])
        bordering.append(self.grid[up][c2])
        bordering.append(self.grid[row][left])
        bordering.append(self.grid[row][right])
        bordering.append(self.grid[down][c1])
        bordering.append(self.grid[down][c2])
        return bordering

        
    

    def evolve_bordering(self,row,column,midvalue):
        if row ==0:
            up = self.height-1
            down = row+1
            c1 = column
            if column == 0:
                    c2 = self.size-1
            else:
                c2= column-1
        elif row == self.height-1:
            up = row-1
            down = 0
            c1 = column
            if column == self.size-1:
                c2 = 0
            else:
                c2=column+1   
        else:
            if row%2==0:
                up = row-1
                down = row+1
                c1 = column
                if column == 0:
                    c2 = self.size-1
                else:
                    c2= column-1
            else:
                up = row-1
                down = row+1
                c1 = column
                if column == self.size-1:
                    c2 = 0
                else:
                    c2=column+1     
        if column ==0:
            right = column +1
            left = self.size-1
        elif column == self.size-1:
            right = 0
            left = column-1
        else:
            right = column +1
            left = column-1
        if self.grid[up][c1]<midvalue:
            self.grid[up][c1]+=1
        if self.grid[up][c2]<midvalue:
            self.grid[up][c2]+=1
        if self.grid[row][left]<midvalue:
            self.grid[row][left]+=1
        if self.grid[row][right]<midvalue:
            self.grid[row][right]+=1
        if self.grid[down][c1]<midvalue:
            self.grid[down][c1]+=1
        if self.grid[down][c2]<midvalue:
            self.grid[down][c2]+=1


    def get_bordering_lowers(self, row, column, midvalue):
        bordering = []
        if row ==0:
            up = self.height-1
            down = row+1
            c1 = column
            if column == 0:
                    c2 = self.size-1
            else:
                c2= column-1
        elif row == self.height-1:
            up = row-1
            down = 0
            c1 = column
            if column == self.size-1:
                c2 = 0
            else:
                c2=column+1   
        else:
            if row%2==0:
                up = row-1
                down = row+1
                c1 = column
                if column == 0:
                    c2 = self.size-1
                else:
                    c2= column-1
            else:
                up = row-1
                down = row+1
                c1 = column
                if column == self.size-1:
                    c2 = 0
                else:
                    c2=column+1     
        if column ==0:
            right = column +1
            left = self.size-1
        elif column == self.size-1:
            right = 0
            left = column-1
        else:
            right = column +1
            left = column-1
        if self.grid[up][c1]<midvalue:
            bordering.append(self.grid[up][c1])
        if self.grid[up][c2]<midvalue:
            bordering.append(self.grid[up][c2])
        if self.grid[row][left]<midvalue:
            bordering.append(self.grid[row][left])
        if self.grid[row][right]<midvalue:
            bordering.append(self.grid[row][right])
        if self.grid[down][c1]<midvalue:
            bordering.append(self.grid[down][c1])
        if self.grid[down][c2]<midvalue:
            bordering.append(self.grid[down][c2])
        if bordering:
            return bordering
        return (midvalue,midvalue)
        



    def evolve(self,passcount):
        rowcount =1
        for row in self.grid:
            rowindex = self.grid.index(row)
            count = 0
            formattedtime = timeformat(time.process_time())
            print(f"Pass {passcount}: Evolution of row {rowcount} began at {formattedtime}")
            rowcount+=1
            for column in row:
                chance = randint(1,100)
                columnindex = count
                if chance >= 99:
                    row[count]+=1
                if column-2>=mean(self.get_bordering_lowers(rowindex,columnindex,column)):
                    average = mean(self.get_bordering_lowers(rowindex,columnindex,column))
                    while (column-average) >=2:
                        self.evolve_bordering(rowindex,columnindex,column)
                        average+=1
                count+=1
                
                
                
                

    def highteir(self):
        high = 0
        for rows in self.grid:
            for column in rows:
                if high<column:
                    high = column
        return high

    def lowteir(self):
        for rows in self.grid:
            low = 15
            for column in rows:
                if low>column:
                    low = column
        return low
                

def timeformat(inputseconds):
    milliseconds = int(inputseconds*100)
    seconds = milliseconds//100
    minutes = seconds//60
    hours = minutes//60
    minutesleft = minutes%60
    secondsleft = seconds%60
    millisecondsleft = milliseconds%100
    return f"{hours}:{minutesleft}:{secondsleft}:{millisecondsleft}"         


                    



map = HexMap(8000)
passcount = 1
while map.highteir() < 15:
    map.evolve(passcount)
    passcount+=1
    
    

out = open("example.txt", "w")
for line in map.grid:
  # write line to output file
  linestr = str(line)
  out.write(linestr)
  out.write(",")
  out.write("\n")
out.close()
print(map.lowteir())
print(map.highteir())
